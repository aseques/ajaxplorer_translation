#!/bin/sh
#This script intends to make a bit easier the live of the translators for ajaxplorer
# it basically copies the content from the live system (where you should make your
# translations and moves it to a repository

if [ ! -e config.inc ] ; then
	echo "ERROR: missing config file (you have to adapt config.inc.dist)"
	exit 0
else
	. ./config.inc
fi

if [ "$1" = "tolocal" ] ; then
	echo "Aplying the translations.."
	DEST=tolocal
elif [ "$1" = "toprod" ] ; then
	echo "Copy the translations to local.."
	DEST=toprod
else
	echo "No actions selected, you have to choose either 'tolocal' or 'toprod'"
	exit 0
fi


#1.- Create the list with all the i18n folders by reading the manifest.xml file
find $WEBROOT -type f -name  "manifest.xml" >/tmp/ajxp_manif
for i in `cat /tmp/ajxp_manif` ; do
	cat $i | grep "i18n namespace=" | egrep -o "path=\"[0-9a-Z\.\/]*\"" | cut -d'=' -f2 | tr -d '"' >>/tmp/ajxp_paths
done >/tmp/ajxp_paths

for i in `cat /tmp/ajxp_paths` ; do 
	echo "$i" >>/tmp/ajxp_list
done >/tmp/ajxp_list
    
#2.- Replicate the folder list to another place (i.e. a git repo)
if [ "$DEST" = "toprod" ] ; then
	echo "cp -Rv $GITROOT/$LANGUAGE  $WEBROOT"
else
	rsync -av $WEBROOT $GITROOT/$LANGUAGE  --files-from=/tmp/ajxp_list --include "$LANGUAGE.php" --include "*/" --exclude "*"
fi
    
#Removing temporary files
rm /tmp/ajxp_list /tmp/ajxp_paths /tmp/ajxp_manif

#Deleting the cache of old translations to see our changes
rm -f $WEBROOT/data/cache/i18n/*
