The script copylang.sh copies the translations from and to  the web server.
You just need to edit the confi.inc file with the details of your install

The script calls are simple:  
To copy the system files to be translated
	copylang.sh tolocal

To copy the translated files to the production environment
	copylang.sh toprod
