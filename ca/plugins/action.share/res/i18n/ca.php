<?php

$mess = array(
    // will be replaced by the application title
    "1" => "%s Descàrrega pública",
    // will be replaced by the filename to download
    "2" => "Feu click a la imatge per descarregar <b>%s</b> al vostre disc",
    "3" => "Contrasenya incorrecta",
    "4" => "Entreu la contrasenya requerida i feu click a la imatge per descarregar <b>%s</b> al vostre disc",
    "5" => "contrasenya",
    "6" => "Deixeu de compartir",
    "7" => "Deixar de compartir no esborrarà el fitxer. Si voleu canviar els paràmetres, re-compartiu el fitxer.",
    "8" => "Els fitxer està compartit",
    "9" => "Feu servir aquest botó per deixar de compartir aquesta carpeta",
    "10"=> "La carpeta és compartida",
    "11"=> "Expiració",
    "12"=> "Contrasenya",
    "13"=> "Si",
    "14"=> "No",
    "15"=> "El fitxer s'ha descarregat %s vegades",
    "16"=> "reinicia",
    "17"=> "Feu click per reiniciar el comptador de descàrregues",
    "18"=> "La carpeta es comparteix com a un nou repositori",
    "19"=> "Les opcions del repositori compartit s'han modificat correctament",
    "20"=> "Ooops, el fitxer que heu solicitat no s'ha trobat! Potser l'han esborrat o ja no us el comparteixen.",
    "21"=> "Expirar despres de (dies): ",
    "22"=> "Descàrregues permeses: ",
    "23"=> "Contrasenya opcional: ",
    "24"=> "Configureu els límits de durada i definiu una contrasenya d'accés al fitxer, llavors feu click a \"Generar\".",
    "25"=> "Sense Limit"
);
